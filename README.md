# GitLab's Private Mesa CI

This is a prototype to show how to customize some of the test sets to
be exercised in a non public extension of the public and upstream
[Mesa CI](https://gitlab.freedesktop.org/tanty/mesa/pipelines), when
it is not possible to share some of the data needed for testing.

## Tracked Mesa branches

The supported target branches are the release ones, for example
`20.0`, and `master`. Any other custom private branch will be
ignored.

This means that pipelines targeting `20.0`, for example, will be
picking our own custom files in the `20.0` branch of the
`mesa-gitlab-ci-private` repository. Any other non-release branch,
will use the files in the `master` branch of the
`mesa-gitlab-ci-private` repository.

## Supported test sets

Currently, the only supported test sets from Mesa's CI to be
customized are the traces list
[traces.yml](https://gitlab.freedesktop.org/tanty/mesa/-/blob/master/.gitlab-ci/traces.yml),
to be exercised by the trace testers, and the
[fossils.yml](https://gitlab.freedesktop.org/tanty/mesa/-/blob/master/.gitlab-ci/fossils.yml)
fossils list, to be exercised by the fossilize testers.

In order to track our own set of custom tests against the upstream
Mesa repository's commit history, we will modify 2 files, in the
proper branch. For every kind of tests set, the file names are the
expected ones but, assuming we want to modify the `traces` tests:

* `data/traces.yml`: This is the file we want our gitlab-runners to
  pick up for trace testing, instead of the upstream one.

* `data/traces_map.yml`: This file tracks versions of our custom test
  set file against specific commits in the same branch in Mesa
  upstream. A minimal map file is as follows:

```
commits:
  -
    mesa-date: "Mon Apr 6 19:15:19 2020 +0000"
    mesa-sha: 20a4b1461bab25af48d73b07ca5bafafc397eb2e
    mesa-subject: "aco: zero-initialize Temp"
    private-file-sha: 61573a43c90bfed469242e83bf85868648cc19de
default:
  private-file-sha: bc6ee4862d80e72020d11ac94e0de5617e7e8e82
```

The `commits` sequence is the list of matching `mesa-sha` commits,
against our own `private-file-sha` commits from which we want to
gather a specific version of the custom test set file.

When retrieving a specific version, the script checks, sequentially,
if the `mesa-sha` commit is an ancestor of the current upstream commit
in the target branch. If that's not the case, it checks with the next
entry in the `commits` sequence. Hence, the `commits` sequence much
maintain a strict sequence by which every next `mesa-sha` entry is an
ancestor of the previous one in the upstream Mesa repository.

Finally, if no ancestor `mesa-sha` commit is found, the script returns
an empty test set list from the default `private-file-sha` commit.

## Setting your custom CI job

First thing would be having the utility script available in our gitlab
runners. We'll copy the `get-test-objects-yml.py` file into a
directory readable by the gitlab-runner. For example
`/home/gitlab-runner/mesa-gitlab-ci-private-lib`.

Then, we will customize, if needed the following VARIABLES in the
copied script:

* `MESA_GITLAB_PROJECT_URL`: The upstream Mesa project or another
  GitLab fork, if we are working against it, instead of against
  upstream.
* `GITLAB_PROJECT_URL`: The GitLab project in which we will be storing
  our own versions of the test set files and map files matching the
  former ones against specific commits from the repository stored at
  the `MESA_GITLAB_PROJECT_URL`.
* `OUTPUT_DIR`: The relative output directory to the GitLab CI
  `CI_PROJECT_DIR` predefined variable in which to store the resulting
  test set files.

Now, assuming a docker executor, we'll add a volume mount of that
directory to `/etc/gitlab-runner/config.toml` so that the docker
container can access it.  You probably have a `volumes = ["/cache"]`
already, so now it would be

```
  volumes = ["/home/gitlab-runner/mesa-gitlab-ci-private-lib:/mesa-gitlab-ci-private", "/cache"]
```

Make sure that your gitlab-runners have some specific tag(s) not shared
by other runners which won't have this customization.

The second step would be adding or modifying a job which exercises one
of the supported test sets to be customized.

For example, a non-customized job which exercises Vulkan traces would
look like this at
[.gitlab-ci.yml](https://gitlab.freedesktop.org/tanty/mesa/-/blob/master/.gitlab-ci.yml):


```
radv-polaris10-traces:
  extends:
    - .traces-test-vk
    - .test-radv
    - .test-manual
  variables:
    DEVICE_NAME: "vk-amd-polaris10"
  tags:
    - polaris10
```

We'll have to identify which is the variable to set for our type of
job. Currently, and for the case of trace tests, that would be
`GET_TRACES_FILE_PATH`.

Then, we will set such variable with the command we want to run in
order to generate the proper set of tests file and return back its
final location.

For our current utility script, the command line to run would be:

```
$ python3 /mesa-gitlab-ci-private/get-test-objects-yml.py traces
```

Notice that the path to the script is the destination mount point of
the new volume entry we added in our gitlab-runners.

We'll also make sure to add the custom tag(s) set in our
gitlab-runners not shared with other non-customized runners. Let's
say, we just used `valve-mesa-gitlab-ci-private`.

Therefore, the job entry now will look like:

```
radv-polaris10-traces:
  extends:
    - .traces-test-vk
    - .test-radv
    - .test-manual
  variables:
    DEVICE_NAME: "vk-amd-polaris10"
    GET_TRACES_FILE_PATH: "python3 /mesa-gitlab-ci-private/get-test-objects-yml.py traces"
  tags:
    - polaris10
    - valve-mesa-gitlab-ci-private
```

All that is left is to add our custom gitlab-runners to the pool of
runners for the upstream Mesa GitLab CI.
