# Copyright (c) 2020 Collabora Ltd
# Copyright © 2020 Valve Corporation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import os
import requests
import sys
import argparse
import yaml

from pathlib import Path
from urllib import parse

MESA_GITLAB_PROJECT_URL = "https://gitlab.freedesktop.org/mesa/mesa"
GITLAB_PROJECT_URL = "https://gitlab.freedesktop.org/tanty/mesa-gitlab-ci-private"
OUTPUT_DIR = "ci-private"
DEBUG=False

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def dprint(*args, **kwargs):
    if DEBUG:
        eprint(*args, **kwargs)

def gitlab_get_file(project_url, repo_commit, file_path):
    url = parse.urlparse(project_url)

    url_path = url.path
    if url_path.startswith("/"):
        url_path = url_path[1:]

    gitlab_api_url = url.scheme + "://" + url.netloc + "/api/v4/projects/" + parse.quote_plus(url_path)

    r = requests.get(gitlab_api_url + "/repository/files/%s/raw?ref=%s" % (parse.quote_plus(file_path), repo_commit))
    r.raise_for_status()

    return r

def gitlab_is_sha_ancestor(project_url, candidate_sha, reference_sha):
    url = parse.urlparse(project_url)

    url_path = url.path
    if url_path.startswith("/"):
        url_path = url_path[1:]

    gitlab_api_url = url.scheme + "://" + url.netloc + "/api/v4/projects/" + parse.quote_plus(url_path)

    r = requests.get(gitlab_api_url + "/repository/merge_base?refs[]=%s&refs[]=%s" % (candidate_sha, reference_sha))
    r.raise_for_status()

    return r.json()['id'] == candidate_sha

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('tests', choices=['traces', 'fossils'],
                        help='the type of tests set that we want to get')

    args = parser.parse_args()

    mandatory_env_vars = ['CI_PROJECT_DIR', 'CI_PROJECT_URL', 'CI_COMMIT_SHA']
    for env_var in mandatory_env_vars:
        if env_var not in os.environ:
            print("The env var %s needs to be set." %
                  (env_var))
            sys.exit(1)

    origin_project = os.environ.get('CI_PROJECT_URL')
    dprint("origin_project:", origin_project)
    origin_sha = os.environ.get('CI_COMMIT_SHA')
    dprint("origin_sha:", origin_sha)

    r = gitlab_get_file(origin_project, origin_sha, "VERSION")
    dprint("VERSION file:\n", r.text)

    reference_test_branch = "master"
    for line in r.text.splitlines():
        if not line.strip().endswith('-devel'):
            reference_test_branch = line.rsplit('.', 1)[0]
        break

    # Get the map file
    traces_map_file = args.tests + "_map.yml"
    r = gitlab_get_file(GITLAB_PROJECT_URL, reference_test_branch, "data/" + traces_map_file)
    dprint(traces_map_file, "file:\n", r.text)
    y = yaml.safe_load(r.content)

    mesa_shas = y['commits']
    private_sha = y['default']['private-file-sha']
    traces_file = "empty_" + args.tests + "_file_please_rebase.yml"
    for mesa_sha in mesa_shas:
        # We could check in the locally cloned repository but current
        # Mesa policy is making a 100 commits depth shallow clone so
        # it is quite likely that our reference won't be already
        # there.
        # Another strategy, though, would be trying to fetch mesa_sha ...
        if gitlab_is_sha_ancestor(origin_project, mesa_sha['mesa-sha'], origin_sha):
            private_sha = mesa_sha['private-file-sha']
            traces_file = args.tests + ".yml"
            break

    r = gitlab_get_file(GITLAB_PROJECT_URL, private_sha, "data/" + traces_file)
    dprint(traces_file, "file:\n", r.text)

    outputdir_path = Path(os.environ.get('CI_PROJECT_DIR')) / OUTPUT_DIR
    outputdir_path.mkdir(parents=True, exist_ok=True)

    outputfile = outputdir_path / traces_file
    with open(str(outputfile), 'w+b') as f:
        f.write(r.content)

    print(outputfile)

    sys.exit(0)

if __name__ == "__main__":
    main()
